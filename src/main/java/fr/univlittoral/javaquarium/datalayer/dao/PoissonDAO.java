package fr.univlittoral.javaquarium.datalayer.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.univlittoral.javaquarium.beans.entities.PoissonDO;

/**
 * Fish DAO
 */
@Component
public class PoissonDAO {
    
    @Autowired
    private EntityManager entityManager;
    
    
    /**
     * Get all fishes
     */
    @SuppressWarnings("unchecked")
    public List<PoissonDO> queryAll(){
        return entityManager.createQuery("Select p from PoissonDO p").getResultList();
    }
}
    