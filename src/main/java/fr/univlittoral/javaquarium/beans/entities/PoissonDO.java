package fr.univlittoral.javaquarium.beans.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity 
@Table(name="P_POISSONS")
public class PoissonDO {

	@Id @GeneratedValue(strategy=GenerationType.AUTO) @Column(name="P_ID")
	private Integer id;
	@Column(name="P_ESPECE") 
	private String espece;
	@Column(name="P_DESC1") 
	private String desc1;
	@Column(name="P_DESC2") 
	private String desc2;
	@Column(name="P_DESC3") 
	private String desc3;
	@Column(name="P_COULEUR") 
	private String couleur;
	@Column(name="P_LARGEUR") 
	private double largeur;
	@Column(name="P_LONGUEUR") 
	private double longueur;
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the espece
	 */
	public String getEspece() {
		return espece;
	}
	/**
	 * @param espece the espece to set
	 */
	public void setEspece(String espece) {
		this.espece = espece;
	}
	/**
	 * @return the desc1
	 */
	public String getDesc1() {
		return desc1;
	}
	/**
	 * @param desc1 the desc1 to set
	 */
	public void setDesc1(String desc1) {
		this.desc1 = desc1;
	}
	/**
	 * @return the desc2
	 */
	public String getDesc2() {
		return desc2;
	}
	/**
	 * @param desc2 the desc2 to set
	 */
	public void setDesc2(String desc2) {
		this.desc2 = desc2;
	}
	/**
	 * @return the desc3
	 */
	public String getDesc3() {
		return desc3;
	}
	/**
	 * @param desc3 the desc3 to set
	 */
	public void setDesc3(String desc3) {
		this.desc3 = desc3;
	}
	/**
	 * @return the couleur
	 */
	public String getCouleur() {
		return couleur;
	}
	/**
	 * @param couleur the couleur to set
	 */
	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}
	/**
	 * @return the largeur
	 */
	public double getLargeur() {
		return largeur;
	}
	/**
	 * @param largeur the largeur to set
	 */
	public void setLargeur(double largeur) {
		this.largeur = largeur;
	}
	/**
	 * @return the longueur
	 */
	public double getLongueur() {
		return longueur;
	}
	/**
	 * @param longueur the longueur to set
	 */
	public void setLongueur(double longueur) {
		this.longueur = longueur;
	}
	/**
	 * @return the prix
	 */
	public double getPrix() {
		return prix;
	}
	/**
	 * @param prix the prix to set
	 */
	public void setPrix(double prix) {
		this.prix = prix;
	}
	@Column(name="P_PRIX")
	private double prix;
	
}
