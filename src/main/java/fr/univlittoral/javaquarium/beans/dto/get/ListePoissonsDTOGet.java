package fr.univlittoral.javaquarium.beans.dto.get;

import java.util.List;

/**
 * object that represent a list of fishes
 */
public class ListePoissonsDTOGet {
	private List<PoissonDTOGet> listPoissons;

	/**
	 * @return the listPoissons
	 */
	public List<PoissonDTOGet> getListPoissons() {
		return listPoissons;
	}

	/**
	 * @param listPoissons the listPoissons to set
	 * @return 
	 */
	public void setListPoissons(List<PoissonDTOGet> listPoissons) {
		this.listPoissons = listPoissons;
	}
}
