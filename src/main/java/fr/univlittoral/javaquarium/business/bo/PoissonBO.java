package fr.univlittoral.javaquarium.business.bo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.univlittoral.javaquarium.beans.dto.get.ListePoissonsDTOGet;
import fr.univlittoral.javaquarium.business.mapping.Mapping;
import fr.univlittoral.javaquarium.datalayer.dao.PoissonDAO;

@Component
public class PoissonBO {
	@Autowired
	private PoissonDAO poissonDao;
	
	@Autowired
	private Mapping mapping;
	
	/**
     * Get all fishes
     */
	public ListePoissonsDTOGet findAll() {
		ListePoissonsDTOGet result = new ListePoissonsDTOGet();
		result.setListPoissons(mapping.map(poissonDao.queryAll()));
		return result;
	}
}
