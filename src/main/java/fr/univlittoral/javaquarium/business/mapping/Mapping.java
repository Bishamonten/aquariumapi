package fr.univlittoral.javaquarium.business.mapping;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import fr.univlittoral.javaquarium.beans.dto.get.PoissonDTOGet;
import fr.univlittoral.javaquarium.beans.entities.PoissonDO;

@Component
public class Mapping {
	public List<PoissonDTOGet> map(final List<PoissonDO> listPoissonDO){
		final List<PoissonDTOGet> result = new ArrayList<PoissonDTOGet>();
		listPoissonDO.forEach(poissonDO -> {
			PoissonDTOGet poisson = new PoissonDTOGet();
			poisson.setNom(poissonDO.getEspece());
			poisson.setDescription(poissonDO.getDesc1());
			poisson.setCouleur(poissonDO.getCouleur());
			poisson.setDimension(poissonDO.getLargeur() + " x " + poissonDO.getLongueur());
			poisson.setPrix(poissonDO.getPrix());
			result.add(poisson);
		});
		return result;
	}

}
