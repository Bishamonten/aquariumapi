package fr.univlittoral.javaquarium.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univlittoral.javaquarium.beans.dto.get.ListePoissonsDTOGet;
import fr.univlittoral.javaquarium.beans.dto.post.PoissonDTOPost;
import fr.univlittoral.javaquarium.business.bo.PoissonBO;

/**
 * Get fish controller
 */
@RestController
@RequestMapping(value = "/api/listepoissons")
public class ListePoissonsController {

	@Autowired
	private PoissonBO poissonBO;

	/**
	 * Get fish list
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ListePoissonsDTOGet get() {
		return poissonBO.findAll();
	}

	/*	*//**
			 * Get fish list
			 *//*
				 * @RequestMapping(method = RequestMethod.POST) public ListePoissonsDTOGet
				 * post(PoissonDTOPost dto) { return poissonBO.add(dto); }
				 */

}
